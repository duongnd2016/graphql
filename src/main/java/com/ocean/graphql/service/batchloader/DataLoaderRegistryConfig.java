package com.ocean.graphql.service.batchloader;

import com.ocean.graphql.service.dto.PostDTO;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class DataLoaderRegistryConfig extends org.dataloader.DataLoaderRegistry {

    @Autowired
    private AllPostsBatchLoader allPostsBatchLoader;

    @PostConstruct
    public void init() {
        DataLoaderOptions disableCacheOptions = DataLoaderOptions.newOptions().setCachingEnabled(false);
        DataLoader<Long, List<PostDTO>> allPostsDataLoader = DataLoader.newDataLoader(allPostsBatchLoader, disableCacheOptions);

        this.register("posts", allPostsDataLoader);
    }
}
