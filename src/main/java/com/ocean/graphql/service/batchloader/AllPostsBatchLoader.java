package com.ocean.graphql.service.batchloader;

import com.ocean.graphql.service.PostService;
import com.ocean.graphql.service.dto.PostDTO;
import org.dataloader.BatchLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Component
public class AllPostsBatchLoader implements BatchLoader<Long, List<PostDTO>> {

    @Autowired
    private PostService postService;

    @Override
    public CompletionStage<List<List<PostDTO>>> load(List<Long> keys) {
        return CompletableFuture.supplyAsync(() -> {

            List<PostDTO> postDTOS = postService.findByAuthorIdIn(keys);
            List<List<PostDTO>> result = new ArrayList<>();

            for(int i = 0; i < keys.size(); i++) {
                result.add(new ArrayList<>());
            }
            for (PostDTO postDTO: postDTOS) {
                int index = keys.indexOf(postDTO.getAuthorId());

                result.get(index).add(postDTO);
            }

            return result;
        });
    }
}
