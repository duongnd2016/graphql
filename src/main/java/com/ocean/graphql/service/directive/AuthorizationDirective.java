package com.ocean.graphql.service.directive;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.idl.SchemaDirectiveWiring;
import graphql.schema.idl.SchemaDirectiveWiringEnvironment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class AuthorizationDirective implements SchemaDirectiveWiring {

    @Override
    public GraphQLFieldDefinition onField(SchemaDirectiveWiringEnvironment<GraphQLFieldDefinition> schemaDirectiveWiringEnv) {

        String targetAuthRole = (String) schemaDirectiveWiringEnv.getDirective().getArgument("role").getValue();

        GraphQLFieldDefinition field = schemaDirectiveWiringEnv.getElement();
        //
        // build a data fetcher that first checks authorisation roles before then calling the original data fetcher
        //
        DataFetcher originalDataFetcher = field.getDataFetcher();
        DataFetcher authDataFetcher = new DataFetcher() {
            @Override
            public Object get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {

                JwtAuthenticationToken contextMap = dataFetchingEnvironment.getContext();

                Collection<? extends GrantedAuthority> authorities = contextMap.getAuthorities();

                if (authorities.contains(new SimpleGrantedAuthority(targetAuthRole))) {

                    return originalDataFetcher.get(dataFetchingEnvironment);

                } else {
                    return null;
                }
            }
        };
        //
        // now change the field definition to have the new authorising data fetcher
        return field.transform(builder -> builder.dataFetcher(authDataFetcher));
    }
}
