package com.ocean.graphql.service.datafetcher;

import com.ocean.graphql.service.AuthorService;
import com.ocean.graphql.service.dto.AuthorDTO;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MutationAuthorDataFetcher implements DataFetcher<AuthorDTO> {

    @Autowired
    private AuthorService authorService;

    @Override
    public AuthorDTO get(DataFetchingEnvironment env) throws Exception {

        Map<String, Object> authorInputMap = env.getArgument("author");

        AuthorDTO authorDTO = AuthorDTO.fromMap(authorInputMap);

        return authorService.save(authorDTO);
    }
}
