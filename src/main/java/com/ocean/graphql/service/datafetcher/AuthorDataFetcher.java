package com.ocean.graphql.service.datafetcher;

import com.ocean.graphql.service.AuthorService;
import com.ocean.graphql.service.dto.AuthorDTO;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class AuthorDataFetcher implements DataFetcher<AuthorDTO> {

    private final AuthorService authorService;

    public AuthorDataFetcher(AuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public AuthorDTO get(DataFetchingEnvironment env) throws Exception {
        Map map = env.getArguments();
        Integer id = (Integer) map.get("id");
        Optional<AuthorDTO> authorDTO = this.authorService.findOne(id.longValue());
        if (authorDTO.isPresent()) {
            return authorDTO.get();
        }
        return null;
    }
}
