package com.ocean.graphql.service.datafetcher;

import com.ocean.graphql.service.AuthorService;
import com.ocean.graphql.service.dto.AuthorDTO;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AllAuthorsDataFetcher implements DataFetcher<List<AuthorDTO>> {

    @Autowired
    private AuthorService authorService;

    @Override
    public List<AuthorDTO> get(DataFetchingEnvironment evn) throws Exception {
        return authorService.findAll();
    }
}
