package com.ocean.graphql.service.datafetcher;

import com.ocean.graphql.service.dto.AuthorDTO;
import com.ocean.graphql.service.dto.PostDTO;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class AllPostsByAuthorDataFetcher implements DataFetcher<CompletableFuture<List<PostDTO>>> {

    @Override
    public CompletableFuture<List<PostDTO>> get(DataFetchingEnvironment env) throws Exception {

        AuthorDTO authorDTO = env.getSource();
        DataLoader<Long, List<PostDTO>> dataLoader = env.getDataLoader("posts");

        Long key = authorDTO.getId();

        return dataLoader.load(key).whenComplete(((postDTOS, throwable) -> {
            if (throwable != null) {
                dataLoader.clear(key);
                throwable.printStackTrace();
            }
        }));
    }
}
