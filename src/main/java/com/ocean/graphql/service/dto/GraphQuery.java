package com.ocean.graphql.service.dto;

import java.io.Serializable;

public class GraphQuery implements Serializable {
    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
