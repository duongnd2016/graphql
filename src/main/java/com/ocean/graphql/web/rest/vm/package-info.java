/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ocean.graphql.web.rest.vm;
