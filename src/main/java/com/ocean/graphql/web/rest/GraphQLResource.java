package com.ocean.graphql.web.rest;

import com.ocean.graphql.service.batchloader.DataLoaderRegistryConfig;
import com.ocean.graphql.service.datafetcher.AllAuthorsDataFetcher;
import com.ocean.graphql.service.datafetcher.AllPostsByAuthorDataFetcher;
import com.ocean.graphql.service.datafetcher.AuthorDataFetcher;
import com.ocean.graphql.service.datafetcher.MutationAuthorDataFetcher;
import com.ocean.graphql.service.directive.AuthorizationDirective;
import com.ocean.graphql.service.dto.GraphQuery;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.dataloader.DataLoaderDispatcherInstrumentation;
import graphql.execution.instrumentation.dataloader.DataLoaderDispatcherInstrumentationOptions;
import graphql.execution.instrumentation.tracing.TracingInstrumentation;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")
public class GraphQLResource {

    private final Logger log = LoggerFactory.getLogger(AuthorResource.class);

    @Value("classpath:query.graphql")
    private Resource queryResource;

    @Value("classpath:entities.graphql")
    private Resource entitiesResource;

    private GraphQL graphQL;

    @Autowired
    private AllAuthorsDataFetcher allAuthorsDataFetcher;

    @Autowired
    private AuthorDataFetcher authorDataFetcher;

    @Autowired
    private MutationAuthorDataFetcher mutationAuthorDataFetcher;

    @Autowired
    private AllPostsByAuthorDataFetcher allPostsByAuthorDataFetcher;

    @Autowired
    private DataLoaderRegistryConfig dataLoaderRegistry;

    @Autowired
    private AuthorizationDirective authorizationDirective;

    @PostConstruct
    public void loadSchema() throws IOException {

        File queryResourceFile = queryResource.getFile();
        File entitiesResourceFile = entitiesResource.getFile();

        SchemaParser schemaParser = new SchemaParser();

        TypeDefinitionRegistry typeDefinitionRegistry = new TypeDefinitionRegistry();
        typeDefinitionRegistry.merge(schemaParser.parse(queryResourceFile));
        typeDefinitionRegistry.merge(schemaParser.parse(entitiesResourceFile));

        RuntimeWiring runtimeWiring = buildRuntimeWriting();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        //
        // this instrumentation implementation will dispatch all the data loaders
        // as each level of the graphql query is executed and hence make batched objects
        // available to the query and the associated DataFetchers
        //
        // In this case we use options to make it keep statistics on the batching efficiency
        //
        DataLoaderDispatcherInstrumentationOptions options = DataLoaderDispatcherInstrumentationOptions
            .newOptions().includeStatistics(true);

        DataLoaderDispatcherInstrumentation dispatcherInstrumentation
            = new DataLoaderDispatcherInstrumentation(options);

        //
        // register instrumentations allow you to inject code
        // that can observe the execution of a query and also change the runtime behaviour.
        //
        List<Instrumentation> chainedList = new ArrayList<>();
        chainedList.add(dispatcherInstrumentation);
        chainedList.add(new TracingInstrumentation());
        ChainedInstrumentation chainedInstrumentation = new ChainedInstrumentation(chainedList);

        //
        // now build your graphql object and execute queries on it.
        // the data loader will be invoked via the data fetchers on the
        // schema fields
        //
        graphQL = GraphQL.newGraphQL(graphQLSchema)
            .instrumentation(chainedInstrumentation)
            .build();
    }

    private RuntimeWiring buildRuntimeWriting() {
        return RuntimeWiring.newRuntimeWiring()
            .directive("auth", authorizationDirective)
            .type("QueryType", typeWiring -> typeWiring
                .dataFetcher("allAuthors", allAuthorsDataFetcher)
                .dataFetcher("author", authorDataFetcher))
            .type("Mutation", typeWiring -> typeWiring
                .dataFetcher("saveAuthor", mutationAuthorDataFetcher))
            .type("Author", typeWiring -> typeWiring
                .dataFetcher("posts", allPostsByAuthorDataFetcher))
            .build();
    }

    @PostMapping("/graphql/query")
    public ResponseEntity query(@RequestBody GraphQuery request, Authentication authentication) {

        String query = request.getQuery();

        ExecutionInput executionInput = ExecutionInput.newExecutionInput()
            .query(query)
            .context(authentication)
            .dataLoaderRegistry(dataLoaderRegistry)
            .build();

        ExecutionResult result = graphQL.execute(executionInput);

        Pattern introspectionQuery = Pattern.compile("query(\\s)*IntrospectionQuery");
        Matcher matcher = introspectionQuery.matcher(query);

        if (!matcher.find() && result.getErrors().isEmpty()) {
            return ResponseEntity.ok(result.getData());
        }

        return ResponseEntity.ok(result);
    }
}
